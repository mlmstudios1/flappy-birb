extends Node2D

@export var pipe_scene: PackedScene

var score = 0
var highscore = 0

@onready var high_score_label = %HighScoreLabel
@onready var hud = %HUD

func _ready():
	loadgame()
	high_score_label.text = "highscore = " + str(highscore)

func new_game():
	score = 0
	hud.update_score(score)
	hud.show_message("Get Ready!")
	%StartTimer.start()
	get_tree().call_group("pipes","queue_free")
	%Music.play()
	
	%FlappyBirb.position = %StartPosition.position
	%FlappyBirb.show()
	%FlappyBirb/Area2D/CollisionShape2D2.set_deferred("disabled", false)
	%FlappyBirb.velocity.y = 0

func _on_start_timer_timeout():
	%SpawnTimer.start()
	%ScoreDelayTimer.start()

func _on_spawn_timer_timeout():
	var dualPipes = pipe_scene.instantiate()
	var pipe_spawn_location = get_node("PipePath/PipeSpawnLocation")
	pipe_spawn_location.progress_ratio = randf()
	dualPipes.position = pipe_spawn_location.position
	add_child(dualPipes)

func _on_score_timer_timeout():
	score += 1
	if (score > highscore):
		highscore = score
	high_score_label.text = "highscore = " + str(highscore)
	hud.update_score(score)
	savegame()

func _on_score_delay_timer_timeout():
	%ScoreTimer.start()

func game_over():
	#%FlappyBirb.hide()
	%ScoreTimer.stop()
	%SpawnTimer.stop()
	%ScoreDelayTimer.stop()
	%StartTimer.stop()
	hud.show_game_over()
	%DeathSound.play()
	%Music.stop()
	savegame()

func _on_flappy_birb_hit():
	game_over()

func _on_hud_start_game():
	new_game()

#Saving highscore

#region Saving and loading related code

const SAVE_PATH = "user://savegame.bin"
#should do users:// after exporting game acc freecodecamp vid, for global variables check vid.

func savegame():
	var file = FileAccess.open(SAVE_PATH ,FileAccess.WRITE)
	var data: Dictionary = {
		"highscore": highscore
	}
	
	var jstr = JSON.stringify(data)
	file.store_line(jstr)

func loadgame():
	var file = FileAccess.open(SAVE_PATH ,FileAccess.READ)
	if FileAccess.file_exists(SAVE_PATH):                               #you can also put '== true:' near end
		if not file.eof_reached():
			var current_line = JSON.parse_string(file.get_line())
			if current_line:
				highscore = current_line["highscore"]

#endregion
