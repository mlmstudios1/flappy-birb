extends CharacterBody2D



@export var JUMP_VELOCITY = -425.0
@onready var sprite_2d = $Sprite2D
signal hit

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var startbuttonhit = false

func _physics_process(delta):
	if startbuttonhit:
	# Add the gravity.
		if not is_on_floor():
			velocity.y += gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("up"):
		velocity = Vector2(0,JUMP_VELOCITY)
		startbuttonhit = true

	#var direction = Input.get_axis("ui_left", "ui_right")
	#if direction:
	#	velocity.x = direction * SPEED
	#else:
	#	velocity.x = move_toward(velocity.x, 0, 12)

	move_and_slide()
	
	#leftAndRight()
	
	upAndDown()
	
	# var isLeft = velocity.x < 0
	#sprite_2d.flip_h = isLeft 

	if velocity.y > 1000:
		velocity.y = 1000

#func leftAndRight(): 
#	if (velocity.x < 0):
#		sprite_2d.flip_h = true
#	elif (velocity.x > 0):
#		sprite_2d.flip_h = false

func _input(event):
	if event is InputEventScreenTouch:
		if event.pressed:
			Input.action_press("up")
			Input.action_release("up")

func upAndDown(): 
# alt code - if(velocity.x > 1 || velocity.x < -1)
	if ((velocity.y) > 1):
		sprite_2d.animation = "falling"
	elif ((velocity.y) < 1):
		sprite_2d.animation = "default"

func _on_area_2d_body_entered(body):
	hit.emit()
	hide()
	$Area2D/CollisionShape2D2.set_deferred("disabled", true)




func _on_hud_start_game():
	startbuttonhit = false
